const Course = require('../models/course')


//create a course
module.exports.add = (params) => {
	let course = new Course({
		name: params.name,
		description: params.description,
		price: params.price
	})

	return course.save().then((course, err) => {
		return (err) ? false : true
	})
}

//get all courses
module.exports.getAll = () => {
	return Course.find({}).then(courses => courses)
}

//get specific course
module.exports.get = (params) => {
	return Course.findById(params.courseId).then(course => course)
}

//update a course
module.exports.updateCourse = (params, body) => {
	let updatedCourse = {
		name : body.name,
		description	: body.description,
		price : body.price
	}

	return Course.findByIdAndUpdate(params.courseId, updatedCourse).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	})


}

//archive a course (change the isActive to false)
module.exports.archiveCourse = (params) => {
	const updates = { isActive: false }

	return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
		return (err) ? false : true
	})
}

