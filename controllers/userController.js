const User = require('../models/user');
const Course = require('../models/course');
const bcrypt = require('bcrypt');
const auth = require('../auth');

module.exports.emailExists = (body) => {
	return User.find({ email: body.email }).then(result => {
		return result.length > 0 ? true : false
	})
}


module.exports.register = (params) =>{
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10)
		//10 = salt/string of characters added to password before hashing
	})


	return user.save().then((user, err)=>{
		return (err) ? false : true
	})
}


module.exports.login = (params) => {
	return User.findOne({ email: params.email }).then( user => {
		if (user === null){
			return false
		}

		//compares password received and hashed password
		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		//the mongoose toObject method converts the mongoose object into a plain js object
		if(isPasswordMatched){
			return { accessToken: auth.createAccessToken(user.toObject()) }
		} else {
			return false
		}

		

	})
}


module.exports.get = (params) =>{
	return User.findById(params.userId).then(user =>{
		//re-assign the password to undefined so it won't retrieved along with other user data
		user.password = undefined
		return user
	})
}

/*module.exports.enroll = (params) =>{
	return User.findById(params.userId).then(user => {
		user.enrollments.push({courseId: params.courseId})

		return user.save().then((user, err)=>{
			return Course.findById(params.courseId).then(course =>{
				course.enrollees.push({userId: params.userId})

				return course.save().then((course, err)=>{
					return (err) ? false : true
				})
			})
		})
	})
}
*/

//Async and await - allow processes to wait for each other
//it improves readability of the coded 
//Async and await brings to the tables is that you can explicitly tell your program to wait for the evaluation of a certain statement before proceeding
module.exports.enroll = async (data) =>{
	//Get the courseId and we save it in the enrollments array of the user
	let userSaveStatus = await User.findById(data.userId).then(user =>{
		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user, error)=>{
			if(error){
				return false;
			}else{
				return true
			}
		})
	})//the userSaveStatus tell us if the course was successfully added to the user

	let courseSaveStatus = await Course.findById(data.courseId).then( course =>{
		course.enrollees.push({userId: data.userId});
		return course.save().then((course, error)=>{
			if(error){
				return false;
			}else{
				return true
			}
		})
	})//the courseSaveStatus tells us if the user was successfully saved to the course

	//We have to check if the courseSaveStatus and userSaveStatus are both successful
	if(userSaveStatus && courseSaveStatus){
		return true;
	}else {
		return false
	}
}

