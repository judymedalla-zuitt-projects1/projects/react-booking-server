const express = require('express');
const router = express.Router();
const UserController = require('../controllers/userController');
const auth = require('../auth');

//to check for existing email

router.post('/email-exists', (req, res)=>{
	UserController.emailExists(req.body).then(result => res.send(result))
});


//register a user
router.post('/', (req, res)=>{
	UserController.register(req.body).then(result => res.send(result))
});


//login a user
router.post('/login', (req, res)=>{
	UserController.login(req.body).then(result => res.send(result))
});

//routes for getting the details of a user
router.get('/details', auth.verify, (req, res)=>{
	const user = auth.decode(req.headers.authorization)
	UserController.get({ userId: user.id }).then(user => res.send(user))
})


//enroll the user to a specific course
router.post('/enroll', auth.verify, (req, res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		//userId comes form the decoded JWT's id field
		courseId: req.body.courseId
		//courseId comes from the request body
	}
	UserController.enroll(data).then(result => res.send(result));
})


module.exports = router