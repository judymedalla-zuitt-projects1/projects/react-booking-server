const express = require('express');
const router = express.Router();
const auth = require('../auth')
const CourseController = require('../controllers/courseController')

//create a course
router.post('/', auth.verify, (req, res) => {
	CourseController.add(req.body).then(result => res.send(result))
})


//get all courses
router.get('/', (req, res) => {
	CourseController.getAll().then(courses => res.send(courses))
})


//get specific course
router.get('/:courseId', (req, res) => {
	const courseId = req.params.courseId
	CourseController.get({ courseId }).then(course => res.send(course)) 
})


//update a course
router.put("/:courseId", auth.verify, (req, res) => { //need for middleware
	CourseController.updateCourse(req.params, req.body).then(result => res.send(result))
})


//route to archive a course
router.delete("/:courseId/archive", auth.verify, (req, res) => { //need for middleware
	CourseController.archiveCourse(req.params).then(result => res.send(result))
})


module.exports = router